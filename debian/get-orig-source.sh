#!/bin/sh

VERSION=1.0.3

rm -rf mummy-$VERSION

# This is tag: 6c99e0759992793678f079c3498a4bd856d8d3dd
# git archive --remote=git://public.kitware.com/mummy.git
# -> fatal: The remote end hung up unexpectedly
# so instead git-clone + rm .git
git clone git://public.kitware.com/mummy.git mummy-$VERSION

rm -rf mummy-$VERSION/.git

GZIP="--best --no-name" tar cfz mummy_$VERSION.orig.tar.gz mummy-$VERSION
